package at.spenger.jpa;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import at.spenger.jpa.model.Artist;
import at.spenger.jpa.model.Playlist;
import at.spenger.jpa.model.Track;
import at.spenger.jpa.service.ArtistService;
import at.spenger.jpa.service.PlayListRepository;
import at.spenger.jpa.service.PlayListService;
import at.spenger.jpa.service.TrackRepository;


@Import(Config.class)
public class Application implements CommandLineRunner {

	@Autowired
	private PlayListService playListService;
	@Autowired
	private ArtistService artistService;
	
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

	@Override
	@Transactional
	public void run(String... arg0) throws Exception {
		testReadArtistService();
		testReadArtistServiceList();
		testReadPlaylist();
	}
	
	private void testReadArtistService() {
		Artist a = artistService.findOne(1);
		System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName() + ": " + a.getName());
	}
	
	private void testReadArtistServiceList() {
		List<Artist> l = artistService.findAll();
		System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName() + ": " + l.get(0).getName());
		System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName() + ": " + l.get(1).getName());
	}
	
	private void testReadPlaylist() {
		List<Playlist> l = playListService.findAll();
		//System.out.println("Make me feel better: " + l.get(0).getTracks().get(0).getName());
		//System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName() + ": " + l.get(0)); // Auskommentiert weil zu viele Tracks.
	}
}

// http://www.journaldev.com/2897/solved-hibernateexception-access-to-dialectresolutioninfo-cannot-be-null-when-hibernate-dialect-not-set