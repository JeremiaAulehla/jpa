package at.spenger.jpa.service;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import at.spenger.jpa.model.Genre;

public interface GenreRepository extends CrudRepository<Genre, Integer> {
	@Query("select g from Genre g where g.name like :name")
	List<Genre> findByNameLike(@Param("name") String name);
	List<Genre> findAll();
}
